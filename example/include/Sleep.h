#pragma once

#ifndef SLEEP_ACTION
#define SLEEP_ACTION

#include "Action.h"
#include <glm\vec3.hpp>

class Sleep : public Action
{
public:
	Sleep(AIBrain* pxOwnerBrain);

	NODE_STATUS Update() override;

private:
	float sleepTime = 3.0f;
	float sleepTimer = 0.f;
};

#endif // SLEEP_ACTION