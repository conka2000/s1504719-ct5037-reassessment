#pragma once

#ifndef ACTIONS
#define ACTIONS

#include "Node.h"

class AIBrain;

class Action : public Node
{
public:
	Action( AIBrain* pxOwner ) : m_pxOwnerBrain( pxOwner ) {}

	NODE_STATUS Update() override = 0;

	AIBrain* GetOwnerBrain() { return m_pxOwnerBrain; }

private:
	AIBrain* m_pxOwnerBrain;
};

#endif // ACTIONS