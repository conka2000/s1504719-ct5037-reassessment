#pragma once

#ifndef SEQUENCE
#define SEQUENCE

#include "Composite.h"

class Sequence : public Composite
{
public:
	Sequence();

	NODE_STATUS Update() override;
};

#endif // SEQUENCE