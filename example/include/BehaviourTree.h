#pragma once
#ifndef BEHAVIOUR_TREE
#define BEHAVIOUR_TREE

class AIBrain;

class BehaviourTree
{
public:
	BehaviourTree(AIBrain* pxOwnerBrain) : m_pxOwnerBrain(pxOwnerBrain) {}

	virtual void Initialise() = 0;

	virtual void Update() = 0;
protected:
	AIBrain* GetOwnerBrain() { return m_pxOwnerBrain; }
private:
	AIBrain* m_pxOwnerBrain;
};

#endif