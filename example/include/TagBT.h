#pragma once
#ifndef TAG_BT
#define TAG_BT

#include "BehaviourTree.h"

class Selector;
class IsEnemyInSight;
class IsRightStatus;
class LoopUntilFailure;
class Inverter;
class Chase;
class Seek;
class RunAway;
class Flee;
class Sleep;
class Sequence;

class LoopUntilFailure;

class TagBT : public BehaviourTree
{
public:
	TagBT(AIBrain* pxOwnerBrain);
	~TagBT();

	void Initialise() override;

	void Update() override;

private:
	Selector* m_pxRootSelector;

	LoopUntilFailure* m_pxLoopUntilFailureChase;
	Sequence* m_pxChaseSequence;
	IsRightStatus* m_pxIsChase;
	Sequence* m_pxIsChaseSequence;
	IsEnemyInSight* m_pxIsEnemyInSightChase;
	Chase* m_pxChase;

	LoopUntilFailure* m_pxLoopUntilFailureSeek;
	Sequence* m_pxSeekSequence;
	IsRightStatus* m_pxIsSeek;
	Sequence* m_pxIsSeekSequence;
	Inverter* m_pxInverterSeek;
	IsEnemyInSight* m_pxIsEnemyInSightSeek;
	Seek* m_pxSeek;

	LoopUntilFailure* m_pxLoopUntilFailureRunAway;
	Sequence* m_pxRunAwaySequence;
	IsRightStatus* m_pxIsRunAway;
	Sequence* m_pxIsRunAwaySequence;
	IsEnemyInSight* m_pxIsEnemyInSightRunAway;
	RunAway* m_pxRunAway;

	LoopUntilFailure* m_pxLoopUntilFailureFlee;
	Sequence* m_pxFleeSequence;
	IsRightStatus* m_pxIsFlee;
	Sequence* m_pxIsFleeSequence;
	Inverter* m_pxInverterFlee;
	IsEnemyInSight* m_pxIsEnemyInSightFlee;	
	Flee* m_pxFlee;

	LoopUntilFailure* m_pxLoopUntilFailureSleep;
	Sequence* m_pxSleepSequence;
	IsRightStatus* m_pxIsSleep;
	Sleep* m_pxSleep;

};

#endif // TAG_BT