#pragma once

#ifndef IS_RIGHT_STATUS
#define IS_RIGHT_STATUS

#include "Condition.h"
#include "MyApplication.h"
#include "AIActor.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/ext.hpp>

class AIBrain;

class IsRightStatus : public Condition
{
public:
	IsRightStatus(AIBrain* pxOwnerBrain, BEHAVIOUR_STATE checkState);

	NODE_STATUS Update() override;

	BEHAVIOUR_STATE checkingState;
};

#endif // IS_RIGHT_STATUS
