#pragma once

#ifndef SEEK_ACTION
#define SEEK_ACTION

#include "Action.h"
#include <glm\vec3.hpp>

class Seek : public Action
{
public:
	Seek(AIBrain* pxOwnerBrain);

	NODE_STATUS Update() override;

private:
	glm::vec3 goToPos;
	bool m_bIsMoving;

	int goToNode;
};

#endif // SEEK_ACTION