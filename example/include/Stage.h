#include "Gizmos.h"
#include "Utilities.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/ext.hpp>
#include <iostream>
// This class is responsible for rendering the arena walls and the navmesh
class Stage
{
public:
	Stage();
	~Stage();

	void Update();

	glm::vec3 *GetWallLines() { return wallLines; }

private:
	float square = 1.0f;

	// Walls are dark grey 
	glm::vec4 wallColour = glm::vec4(0.5, 0.5, 0.5, 1);

	glm::vec3 wallLines[20] =
	{
		glm::vec3(-20,0,5.5),
		glm::vec3(-20,0,4.5),
		glm::vec3(-12,0,4.5),
		glm::vec3(-12,0,5.5),
		
		glm::vec3(-6,0,5.5),
		glm::vec3(-6,0,4.5),
		glm::vec3(11,0,4.5),
		glm::vec3(11,0,5.5),

		glm::vec3(-6,0,-7),
		glm::vec3(-6,0,-16),
		glm::vec3(-4,0,-16),
		glm::vec3(-4,0,-7),

		glm::vec3(8.5,0,20),
		glm::vec3(8.5,0,14),
		glm::vec3(9.5,0,14),
		glm::vec3(9.5,0,20),

		glm::vec3(8.5,0,-4),
		glm::vec3(8.5,0,-16),
		glm::vec3(9.5,0,-16),
		glm::vec3(9.5,0,-4)

	};

};