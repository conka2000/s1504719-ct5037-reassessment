#pragma once

#ifndef SELECTOR
#define SELECTOR

#include "Composite.h"

class Selector : public Composite
{
public:
	Selector();

	NODE_STATUS Update() override;
};

#endif // SELECTOR