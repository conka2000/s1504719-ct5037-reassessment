#include "NavMesh.h"
#include "Gizmos.h"
#include "MyApplication.h"

NavMesh::NavMesh()
{
	OnCreate();
}

NavMesh::~NavMesh()
{

}

void NavMesh::OnCreate()
{
	
}

void NavMesh::Update(float a_deltaTime)
{
	//std::deque<NavNodeQuad> copyQueue = myNodes;
	//while (!copyQueue.empty())
	//{
	//	NavNodeQuad drawNode = copyQueue.at(copyQueue.size() - 1);
	//	copyQueue.pop_back();
	//
	//	int leastX = drawNode.vertices[0].x;
	//	int mostX = drawNode.vertices[0].x;
	//	for (int i = 1; i < 4; i++)
	//	{
	//		if (drawNode.vertices[i].x < leastX)
	//		{
	//			leastX = drawNode.vertices[i].x;
	//		}
	//		else if (drawNode.vertices[i].x > mostX)
	//		{
	//			mostX = drawNode.vertices[i].x;
	//		}
	//	}
	//
	//	int leastZ = drawNode.vertices[0].z;
	//	int mostZ = drawNode.vertices[0].z;
	//	for (int i = 1; i < 4; i++)
	//	{
	//		if (drawNode.vertices[i].z < leastZ)
	//		{
	//			leastZ = drawNode.vertices[i].z;
	//		}
	//		else if (drawNode.vertices[i].z > mostZ)
	//		{
	//			mostZ = drawNode.vertices[i].z;
	//		}
	//	}
	//
	//	Gizmos::addBox(drawNode.position, glm::vec3((mostX - leastX) / 2, 1, (mostZ - leastZ) / 2), true, glm::vec4(1, 0, 0.5, 1));
	//}
}

void NavMesh::AddQuadNode(glm::vec3 q_Vertices[4])
{
	NavNodeQuad newNode;
	newNode.navMeshID = nodeCount;
	int newID = newNode.navMeshID;
	newNode.vertices[0] = q_Vertices[0];
	newNode.vertices[1] = q_Vertices[1];
	newNode.vertices[2] = q_Vertices[2];
	newNode.vertices[3] = q_Vertices[3];
	newNode.position = (q_Vertices[0] + q_Vertices[1] + q_Vertices[2] + q_Vertices[3]) * 0.25;

	bool alreadyMatched;
	std::deque<NavNodeQuad> copyQueue = myNodes;

	//ERRORED
	if (nodeCount > 0)
	{
		//for (int i = 0; i < myNodes.size(); i++)
		while(!myNodes.empty())
		{
			alreadyMatched = false;
			NavNodeQuad testingNode = myNodes.at(myNodes.size() - 1);	
			myNodes.pop_back();
			copyQueue.pop_back();
			int testID = testingNode.navMeshID;
			for (int j = 0; j < 4; j++)
			{
				for (int k = 0; k < 4; k++)
				{
					for (int l = 0; l < 4; l++)
					{
						for (int m = 0; m < 4; m++)
						{
							if (lineComparer.LinesIntersect(newNode.vertices[j], newNode.vertices[k], testingNode.vertices[l], testingNode.vertices[m]))
							{
								for (int o = 0; o < newNode.neighbourCount; o++)
								{
									if (newNode.neighbours[o] == testID)
									{
										alreadyMatched = true;
									}
								}
								if (!alreadyMatched)
								{
									newNode.neighbourCount++;
									testingNode.neighbourCount++;
									if (newNode.neighbourCount > 4)
									{
										newNode.neighbourCount = 4;
									}
									if (testingNode.neighbourCount > 4)
									{
										testingNode.neighbourCount = 4;
									}
									newNode.neighbours[newNode.neighbourCount - 1] = testID;
									testingNode.neighbours[testingNode.neighbourCount - 1] = newID;									
								}
							}
						}
					}
				}
			}
			copyQueue.push_front(testingNode);
		}
	}
	copyQueue.push_back(newNode);
	myNodes = copyQueue;
	nodeCount++;
}

//Works for non diagonal rectangles
int NavMesh::GetNodeQuadIDOfPoint(glm::vec3 p_Position)
{
	int returnNumber = -1;
	std::deque<NavNodeQuad> copyQueue = myNodes;
	while (!copyQueue.empty())
	{
		NavNodeQuad testNode = copyQueue.at(copyQueue.size() - 1);
		copyQueue.pop_back();
		int leastX = testNode.vertices[0].x;
		int mostX = testNode.vertices[0].x;
		for (int i = 1; i < 4; i++)
		{
			if (testNode.vertices[i].x < leastX)
			{
				leastX = testNode.vertices[i].x;
			}
			else if (testNode.vertices[i].x > mostX)
			{
				mostX = testNode.vertices[i].x;
			}
		}

		int leastZ = testNode.vertices[0].z;
		int mostZ = testNode.vertices[0].z;
		for (int i = 1; i < 4; i++)
		{
			if (testNode.vertices[i].z < leastZ)
			{
				leastZ = testNode.vertices[i].z;
			}
			else if (testNode.vertices[i].z > mostZ)
			{
				mostZ = testNode.vertices[i].z;
			}
		}

		if (p_Position.x >= leastX && p_Position.x <= mostX && p_Position.z >= leastZ && p_Position.z <= mostZ)
		{
			returnNumber = testNode.navMeshID;
			break;
		}
	}
	//myNodes = copyQueue;
	return returnNumber;
}

glm::vec4 NavMesh::GetNeighboursOf(int navID)
{
	glm::vec4 returnVec = glm::vec4(-2,-2,-2,-2);
	std::deque<NavNodeQuad> copyQueue = myNodes;
	while (!copyQueue.empty())
	{
		NavNodeQuad testNode = copyQueue.at(copyQueue.size() - 1);
		copyQueue.pop_back();
		if (testNode.navMeshID == navID)
		{
			returnVec = glm::vec4(testNode.neighbours[0], testNode.neighbours[1], testNode.neighbours[2], testNode.neighbours[3]);
			break;
		}
	}
	return returnVec;
}

int NavMesh::GetNeighbourCountOf(int navID)
{
	std::deque<NavNodeQuad> copyQueue = myNodes;
	while (!copyQueue.empty())
	{
		NavNodeQuad testNode = copyQueue.at(copyQueue.size() - 1);
		copyQueue.pop_back();
		if (testNode.navMeshID == navID)
		{
			return testNode.neighbourCount;
			break;
		}
	}
}

// Returns (0,100,0) if other node is not a neighbour
glm::vec3 NavMesh::GetEntranceToNextNode(int myID, int otherID)
{
	NavNodeQuad myNode;
	NavNodeQuad otherNode;
	std::deque<NavNodeQuad> copyQueue = myNodes;
	while (!copyQueue.empty())
	{
		NavNodeQuad testNode = copyQueue.at(copyQueue.size() - 1);
		copyQueue.pop_back();
		if (testNode.navMeshID == myID)
		{
			myNode = testNode;
		}
		else if (testNode.navMeshID == otherID)
		{
			otherNode = testNode;
		}

		if (myNode.navMeshID > -1 && otherNode.navMeshID > -1)
		{
			break;
		}
	}

	//for (int i = 0; i < 4; i++)
	//{
	//	for (int j = 0; j < 4; j++)
	//	{
	//		for (int k = 0; k < 4; k++)
	//		{
	//			for (int m = 0; m < 4; m++)
	//			{
	//				if (lineComparer.PointOnLine(myNode.vertices[i], otherNode.vertices[j], otherNode.vertices[k])
	//					&&
	//					lineComparer.PointOnLine(myNode.vertices[m], otherNode.vertices[j], otherNode.vertices[k]))
	//				{
	//					if (myNode.vertices[i].x == myNode.vertices[m].x)
	//					{
	//						Gizmos::addBox(glm::vec3(myNode.vertices[i].x, 0, (myNode.vertices[i].z + myNode.vertices[m].z) / 2), glm::vec3(0.5, 1, 1), true, glm::vec4(1,0,1,1));
	//						return glm::vec3(myNode.vertices[i].x, 0, (myNode.vertices[i].z + myNode.vertices[m].z) / 2);
	//						break;
	//					}
	//					else if (myNode.vertices[i].z == myNode.vertices[m].z)
	//					{
	//						Gizmos::addBox(glm::vec3((myNode.vertices[i].x + myNode.vertices[m].x) / 2, 0, myNode.vertices[i].z), glm::vec3(0.5, 1, 1), true, glm::vec4(1, 0, 1, 1));
	//						return glm::vec3((myNode.vertices[i].x + myNode.vertices[m].x) / 2, 0, myNode.vertices[i].z);
	//						break;
	//					}
	//				}
	//			}
	//		}
	//	}
	//}
	//Gizmos::addBox(glm::vec3(0,5,0), glm::vec3(0.5, 1, 1), true, glm::vec4(1, 0, 1, 1));
	//return glm::vec3(0,5,0);

	return otherNode.position;
}
