// This files header.
#include "AIBrain.h"

// Other includes.
#include "AIActor.h"
#include "Composite.h"
#include "Inverter.h"
#include "IsChargeLow.h"
#include "Patrol.h"
#include "PatrolAndChargeBT.h"
#include "Recharge.h"
#include "Selector.h"
#include "Sequence.h"

AIBrain::AIBrain( AIActor* pxOwnerActor )
	: m_pxOwnerActor( pxOwnerActor )
	, m_pxCurrentBT( new PatrolAndChargeBT( this ) )
{
	m_pxCurrentBT->Initialise();
}

AIBrain::~AIBrain()
{
	delete m_pxCurrentBT;
}

void AIBrain::Update()
{
	m_pxCurrentBT->Update();
}