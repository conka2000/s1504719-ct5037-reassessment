// This file's header.
#include "IsRightStatus.h"

// Other headers.
#include "AIBrain.h"
#include "AIActor.h"

IsRightStatus::IsRightStatus(AIBrain* pxOwnerBrain, BEHAVIOUR_STATE checkState) : Condition(pxOwnerBrain)
{
	checkingState = checkState;
}

NODE_STATUS IsRightStatus::Update()
{
	if (GetOwnerBrain() && GetOwnerBrain()->GetOwnerActor())
	{
		AIActor* pxOwnerActor = GetOwnerBrain()->GetOwnerActor();
		if (pxOwnerActor->GetCurrentState() == checkingState)
		{
			return SUCCESS;
		}
		else
		{
			return FAILURE;
		}
	}
}