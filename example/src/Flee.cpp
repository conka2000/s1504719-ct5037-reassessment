// This file's header.
#include "Flee.h"

// Other headers.
#include "AIActor.h"
#include "AIBrain.h"
#include "MyApplication.h"

Flee::Flee(AIBrain* pxOwnerBrain)
	: Action(pxOwnerBrain)
{
}

NODE_STATUS Flee::Update()
{
	if (!GetOwnerBrain() || !GetOwnerBrain()->GetOwnerActor())
	{
		return FAILURE;
	}

	AIActor* pxOwnerActor = GetOwnerBrain()->GetOwnerActor();

	if (!m_bIsMoving)
	{
		glm::vec3 myPos = pxOwnerActor->GetCurrentPosition();

		int myID = pxOwnerActor->GetCurrentNodeID();

		int otherID;

		float maxDistance = 0;

		glm::vec4 myNeighboursV = pxOwnerActor->GetNavMesh()->GetNeighboursOf(myID);
		int myNeighbours[4] = { myNeighboursV.r, myNeighboursV.g, myNeighboursV.b, myNeighboursV.a };

		for (int i = 0; i < pxOwnerActor->GetNavMesh()->GetNeighbourCountOf(myID); i++)
		{
			float dist = MyApplication::FindDistanceBetween(myPos, pxOwnerActor->GetNavMesh()->GetEntranceToNextNode(myID, myNeighbours[i]));
			if (dist > maxDistance && myNeighbours[i] != pxOwnerActor->GetPreviousNode())
			{
				maxDistance = dist;
				otherID = myNeighbours[i];
			}
		}

		goToPos = pxOwnerActor->GetNavMesh()->GetEntranceToNextNode(myID, otherID);
		if (goToPos.y >= 100)
		{
			otherID = pxOwnerActor->GetNavMesh()->GetNeighboursOf(myID).y;
			goToPos = pxOwnerActor->GetNavMesh()->GetEntranceToNextNode(myID, otherID);
		}

		pxOwnerActor->SetPreviousNode(myID);
		goToNode = otherID;

		pxOwnerActor->RequestMove(goToPos);
		m_bIsMoving = true;
	}
	else if (
		MyApplication::FindDistanceBetween(pxOwnerActor->GetCurrentPosition(), goToPos) < 2.f
		&&
		pxOwnerActor->GetNavMesh()->GetNodeQuadIDOfPoint(pxOwnerActor->GetCurrentPosition()) == goToNode
		)
	{
		pxOwnerActor->RequestStop();
		m_bIsMoving = false;
		return SUCCESS;
	}

	return RUNNING;
}