#include "AIActor.h"
#include "Gizmos.h"
#include "MyApplication.h"

AIActor::AIActor()
	: m_xCurrentPosition(0.0f, 0.0f, 0.0f)
	, m_xMoveToPosition(m_xCurrentPosition)
	, m_xMoveToDirection(0.0f, 0.0f, 0.0f)
	, m_xCurrentColour(1.0f, 1.0f, 1.0f, 1.0f)
	, m_bMoveRequested(false)
	, m_xBrain(this)
{
	
}

AIActor::~AIActor()
{

}

void AIActor::Start(bool tagged, glm::vec3 startPos, AIActor* enemy)
{
	if (tagged)
	{
		currentState = RUNAWAY;
	}
	else
	{
		currentState = CHASE;
	}

	m_xCurrentPosition = startPos;
	m_xMoveToPosition = m_xCurrentPosition;
	
	m_xEnemy = enemy;
}

void AIActor::Update(float a_deltaTime)
{
	if (currentState == CHASE)
	{
		m_xCurrentColour = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	}
	else if (currentState == SEEK)
	{
		m_xCurrentColour = glm::vec4(1.0f, 0.0f, 1.0f, 1.0f);
	}
	else if (currentState == RUNAWAY)
	{
		m_xCurrentColour = glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);
	}
	else if (currentState == FLEE)
	{
		m_xCurrentColour = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
	}
	else
	{
		m_xCurrentColour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
	}
	// Always think before acting...
	m_xBrain.Update(a_deltaTime);

	// Move if we are flagged to be moving.
	if (m_bMoveRequested)
	{
		// If not then move the current position.
		m_xCurrentPosition += (m_xMoveToDirection * speed);
	}

	// Update our sphere with the new info.
	Gizmos::addSphere(m_xCurrentPosition, 10, 10, 1.0f, m_xCurrentColour);
}

void AIActor::RequestMove(const glm::vec3& xPosition)
{
	// Flag the move had been requested.
	m_bMoveRequested = true;

	// Cache our desired position.
	m_xMoveToPosition = xPosition;

	// Make sure the sphere can't go in the ground.
	if (m_xMoveToPosition.y < 1.0f)
	{
		m_xMoveToPosition.y = 1.0f;
	}

	// If not calculate the direction.
	glm::vec3 xDirection = m_xMoveToPosition - m_xCurrentPosition;
	xDirection = glm::normalize(xDirection);

	// Cache the direction to our desire
	m_xMoveToDirection = xDirection;
}

void AIActor::RequestStop()
{
	// Unflag movement.
	m_bMoveRequested = false;
}

void AIActor::SetNavMesh(NavMesh* theMesh)
{
	mapMesh = theMesh;
}

void AIActor::SetState(BEHAVIOUR_STATE theState)
{
	if (theState == CHASE)
	{
		speed = 0.115f;
	}
	else
	{
		speed = 0.1f;
	}
	currentState = theState;
}

void AIActor::Tagged()
{
	BEHAVIOUR_STATE state = currentState;
	if (currentState == CHASE || currentState == SEEK)
	{
		SetState(RUNAWAY);
	}
	else if (currentState == RUNAWAY || currentState == FLEE)
	{
		SetState(SLEEP);
	}
}

float AIActor::Sleeping(float a_deltaTime)
{
	sleepTimer += a_deltaTime;
	return sleepTimer;
}