// This file's header.
#include "Inverter.h"

Inverter::Inverter(Node* pxChild) : Decorator(pxChild)
{

}

NODE_STATUS Inverter::Update()
{
	Node* pxChild = GetChildNode();
	if (pxChild)
	{
		NODE_STATUS eStatus = pxChild->Update();
		if (eStatus == SUCCESS)
		{
			return FAILURE;
		}
		else if (eStatus == FAILURE)
		{
			return SUCCESS;
		}
		else
		{
			return eStatus;
		}
	}
}

