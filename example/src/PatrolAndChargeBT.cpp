// This file's header.
#include "PatrolAndChargeBT.h"

#include "Selector.h"
#include "IsChargeLow.h"
#include "Inverter.h"
#include "Recharge.h"
#include "Sequence.h"
#include "Patrol.h"

#include "LoopUntilFailure.h"

PatrolAndChargeBT::PatrolAndChargeBT(AIBrain* pxOwnerBrain) : BehaviourTree(pxOwnerBrain)
{
	m_pxIsChargeLow = new IsChargeLow(GetOwnerBrain());
	m_pxInverter = new Inverter(m_pxIsChargeLow);
	m_pxPatrol = new Patrol(GetOwnerBrain());

	m_pxMoveSequence = new Sequence();
	m_pxMoveSequence->AddChild(m_pxInverter);
	m_pxMoveSequence->AddChild(m_pxPatrol);

	//m_pxLoopUntilFailure = new LoopUntilFailure(m_pxMoveSequence);

	m_pxRecharge = new Recharge(GetOwnerBrain());

	m_pxRootSelector = new Selector();
	m_pxRootSelector->AddChild(m_pxMoveSequence);
	m_pxRootSelector->AddChild(m_pxRecharge);
}

PatrolAndChargeBT::~PatrolAndChargeBT()
{
	delete m_pxRootSelector;
	delete m_pxIsChargeLow;
	delete m_pxInverter;
	delete m_pxRecharge;
	delete m_pxMoveSequence;
	delete m_pxPatrol;

	delete m_pxLoopUntilFailure;
}

void PatrolAndChargeBT::Initialise()
{

}

void PatrolAndChargeBT::Update()
{
	m_pxRootSelector->Update();
}

