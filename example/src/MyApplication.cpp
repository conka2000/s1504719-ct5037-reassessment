#include "MyApplication.h"
#include "Gizmos.h"
#include "Utilities.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/ext.hpp>
#include <iostream>
#include "ChargeArea.h"


#define DEFAULT_SCREENWIDTH 1280
#define DEFAULT_SCREENHEIGHT 720

ChargeArea MyApplication::s_xChargeArea;

MyApplication::MyApplication()
{

}

MyApplication::~MyApplication()
{

}

bool MyApplication::onCreate()
{
	// initialise the Gizmos helper class
	Gizmos::create();
	
	// create a world-space matrix for a camera
	m_cameraMatrix = glm::inverse( glm::lookAt(glm::vec3(10,10,10),glm::vec3(0,0,0), glm::vec3(0,1,0)) );
	
	// create a perspective projection matrix with a 90 degree field-of-view and widescreen aspect ratio
	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f, DEFAULT_SCREENWIDTH/(float)DEFAULT_SCREENHEIGHT, 0.1f, 1000.0f);

	// set the clear colour and enable depth testing and backface culling
	glClearColor(0.25f,0.25f,0.25f,1.f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	glm::vec3 topRight[4] = { glm::vec3(-20, 0, 4.5), glm::vec3(-20, 0, -20), glm::vec3(-6, 0, 4.5), glm::vec3(-6, 0, -20) };
	m_xNavMesh.AddQuadNode(topRight);
	glm::vec3 topLeft[4] = { glm::vec3(-20, 0, 5.5), glm::vec3(-20, 0, 20), glm::vec3(8.5, 0, 20), glm::vec3(8.5, 0, 5.5) };
	m_xNavMesh.AddQuadNode(topLeft);
	glm::vec3 bottomLeft[4] = { glm::vec3(9.5, 0, 5.5), glm::vec3(20, 0, 5.5), glm::vec3(9.5, 0, 20), glm::vec3( 20, 0, 20) };
	m_xNavMesh.AddQuadNode(bottomLeft);
	glm::vec3 bottomRight[4] = { glm::vec3(9.5, 0, -20), glm::vec3(9.5, 0, 4.5), glm::vec3(20, 0, -20), glm::vec3(20, 0, 4.5) };
	m_xNavMesh.AddQuadNode(bottomRight);
	glm::vec3 middle[4] = { glm::vec3(8.5, 0, -20), glm::vec3(-4, 0, 4.5), glm::vec3(8.5, 0, 4.5), glm::vec3(-4, 0, -20) };
	m_xNavMesh.AddQuadNode(middle);
	glm::vec3 corridorBottomLeftRight[4] = { glm::vec3(11, 0, 5.5), glm::vec3(11, 0, 4.5), glm::vec3(20, 0, 4.5), glm::vec3(20, 0, 5.5) };
	m_xNavMesh.AddQuadNode(corridorBottomLeftRight);
	glm::vec3 corridorTopBottomLeft[4] = { glm::vec3(9.5, 0, 14), glm::vec3(9.5, 0, 5.5), glm::vec3(8.5, 0, 5.5), glm::vec3(8.5, 0, 14) };
	m_xNavMesh.AddQuadNode(corridorTopBottomLeft);
	glm::vec3 corridorMiddleBottomRightLeft[4] = { glm::vec3(9.5, 0, -4), glm::vec3(9.5, 0, 4.5), glm::vec3(8.5, 0, 4.5), glm::vec3(8.5, 0, -4) };
	m_xNavMesh.AddQuadNode(corridorMiddleBottomRightLeft);
	glm::vec3 corridorMiddleBottomRightRight[4] = { glm::vec3(9.5, 0, -20), glm::vec3(9.5, 0, -16), glm::vec3(8.5, 0, -16), glm::vec3(8.5, 0, -20) };
	m_xNavMesh.AddQuadNode(corridorMiddleBottomRightRight);
	glm::vec3 corridorMiddleTopRightRight[4] = { glm::vec3(-4, 0, -20), glm::vec3(-4, 0, -16), glm::vec3(-6, 0, -16), glm::vec3(-6, 0, -20) };
	m_xNavMesh.AddQuadNode(corridorMiddleTopRightRight);
	glm::vec3 corridorMiddleTopRightLeft[4] = { glm::vec3(-4, 0, -7), glm::vec3(-4, 0, 4.5), glm::vec3(-6, 0, -7), glm::vec3(-6, 0, 4.5) };
	m_xNavMesh.AddQuadNode(corridorMiddleTopRightLeft);
	glm::vec3 corridorTopLeftRight[4] = { glm::vec3(-6, 0, 5.5), glm::vec3(-6, 0, 4.5), glm::vec3(-12, 0, 4.5), glm::vec3(-12, 0, 5.5) };
	m_xNavMesh.AddQuadNode(corridorTopLeftRight);

	srand(time(NULL));
    bool randomTagged = rand() % 2;

	glm::vec3 startPos1;

	do
	{
		startPos1 = glm::vec3(rand() % 38 - 19, 1, rand() % 38 - 19);
	} while (m_xNavMesh.GetNodeQuadIDOfPoint(startPos1) < 0);

	glm::vec3 startPos2;

	do
	{
		startPos2 = glm::vec3(rand() % 38 - 19, 1, rand() % 38 - 19);
	} while (m_xNavMesh.GetNodeQuadIDOfPoint(startPos2) < 0 && FindDistanceBetween(startPos1, startPos2) < 3.f);

	m_xOurActor.Start(randomTagged, startPos1, &m_xOurActor2);
	
    m_xOurActor2.Start(!randomTagged, startPos2, &m_xOurActor);

	m_xOurActor.SetNavMesh(&m_xNavMesh);

	m_xOurActor2.SetNavMesh(&m_xNavMesh);

	return true;
}

void MyApplication::Update(float a_deltaTime)
{
	// update our camera matrix using the keyboard/mouse
	Utility::freeMovement( m_cameraMatrix, a_deltaTime, 10 );

	// clear all gizmos from last frame
	Gizmos::clear();
	
	// add an identity matrix gizmo
	Gizmos::addTransform( glm::mat4(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1) );

	// Floor box.
	//Gizmos::addBox(glm::vec3(0.0f, -0.2f, 0.0f), glm::vec3(20.0f, 0.2f, 20.0f), true, glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	// Draw decoration
	m_xStage.Update();

	// Update our actors
	m_xOurActor.Update(a_deltaTime);
	m_xOurActor2.Update(a_deltaTime);

	// Update navigation mesh
	m_xNavMesh.Update(a_deltaTime);

	glm::vec3 h = m_xNavMesh.GetEntranceToNextNode(0, 1);

	// quit our application when escape is pressed
	if (glfwGetKey(m_window,GLFW_KEY_ESCAPE) == GLFW_PRESS)
		quit();
}

void MyApplication::Draw()
{
	// clear the backbuffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// get the view matrix from the world-space camera matrix
	glm::mat4 viewMatrix = glm::inverse( m_cameraMatrix );
	
	// draw the gizmos from this frame
	Gizmos::draw(viewMatrix, m_projectionMatrix);
}

void MyApplication::Destroy()
{
	Gizmos::destroy();
}

float MyApplication::FindDistanceBetween(glm::vec3 xPos1, glm::vec3 xPos2)
{
	glm::vec3 xDiff(xPos1 - xPos2);
	return sqrtf(glm::dot(xDiff, xDiff));
}