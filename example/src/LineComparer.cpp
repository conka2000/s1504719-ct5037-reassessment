#include "LineComparer.h"
#include "Gizmos.h"
#include "MyApplication.h"

LineComparer::LineComparer()
{

}

LineComparer::~LineComparer()
{

}

bool LineComparer::CCW(glm::vec3 point1, glm::vec3 point2, glm::vec3 point3)
{
	return (point3.z - point1.z) * (point2.x - point1.x) > (point2.z - point1.z) * (point3.x - point1.x);
}

bool LineComparer::LinesIntersect(glm::vec3 line1point1, glm::vec3 line1point2, glm::vec3 line2point1, glm::vec3 line2point2)
{
	return CCW(line1point1, line2point1, line2point2) != CCW(line1point2, line2point1, line2point2) && CCW(line1point1, line1point2, line2point1) != CCW(line1point1, line1point2, line2point2);
}

// Not for diagonal lines
bool LineComparer::PointOnLine(glm::vec3 point, glm::vec3 linePoint1, glm::vec3 linePoint2)
{
	if (linePoint1.x == linePoint2.x)
	{
		if (linePoint1.z > linePoint2.z)
		{
			if (point.z < linePoint1.z && point.z > linePoint2.z)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if (linePoint1.z < linePoint2.z)
		{
			if (point.z > linePoint1.z && point.z < linePoint2.z)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	else if (linePoint1.z == linePoint2.z)
	{
		if (linePoint1.x > linePoint2.x)
		{
			if (point.x < linePoint1.x && point.x > linePoint2.x)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if (linePoint1.x < linePoint2.x)
		{
			if (point.x > linePoint1.x && point.x < linePoint2.x)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	else
	{
		return false;
	}
}