// This file's header.
#include "Sleep.h"

// Other headers.
#include "AIActor.h"
#include "AIBrain.h"
#include "MyApplication.h"

#include <time.h>

Sleep::Sleep(AIBrain* pxOwnerBrain)
	: Action(pxOwnerBrain)
{
}

NODE_STATUS Sleep::Update()
{
	if (!GetOwnerBrain() || !GetOwnerBrain()->GetOwnerActor())
	{
		return FAILURE;
	}

	AIActor* pxOwnerActor = GetOwnerBrain()->GetOwnerActor();

	if (sleepTimer <= 0)
	{
		pxOwnerActor->RequestStop();
	}

	sleepTimer += Utility::tickTimer();

	if (sleepTimer > sleepTime)
	{
		sleepTimer = 0.f;
		pxOwnerActor->SetState(CHASE);
		return FAILURE;
	}

	return RUNNING;
}