// This file's header.
#include "IsEnemyInSight.h"

// Other headers.
#include "AIBrain.h"
#include "AIActor.h"

IsEnemyInSight::IsEnemyInSight(AIBrain* pxOwnerBrain) : Condition(pxOwnerBrain)
{
	for (int i = 0; i < 20; i++)
	{ 
		pxWallLines[i] = mapStage.GetWallLines()[i];
	}
}

NODE_STATUS IsEnemyInSight::Update()
{
	if (GetOwnerBrain() && GetOwnerBrain()->GetOwnerActor())
	{
		AIActor* pxOwnerActor = GetOwnerBrain()->GetOwnerActor();

		glm::vec3 myPosition = pxOwnerActor->GetCurrentPosition();
		glm::vec3 enemyPosition = pxOwnerActor->GetEnemyActor()->GetCurrentPosition();

		int myID = pxOwnerActor->GetCurrentNodeID();
		int otherID = pxOwnerActor->GetEnemyCurrentNodeID();

		if (myID == otherID)
		{
			if (pxOwnerActor->GetCurrentState() == SEEK)
			{
				pxOwnerActor->RequestStop();
				pxOwnerActor->SetState(CHASE);
			}
			else if (pxOwnerActor->GetCurrentState() == FLEE)
			{
				pxOwnerActor->RequestStop();
				pxOwnerActor->SetState(RUNAWAY);
			}
			return SUCCESS;			
		}
		else
		{
			for (int i = 0; i < 24; i++)
			{
				if (i == 3 || i == 7 || i == 11 || i == 15 || i == 19)
				{
					if (lineComparer.LinesIntersect(myPosition, enemyPosition, pxWallLines[i], pxWallLines[i - 3]))
					{
						if (pxOwnerActor->GetCurrentState() == CHASE)
						{
							pxOwnerActor->RequestStop();
							pxOwnerActor->SetState(SEEK);
						}
						else if (pxOwnerActor->GetCurrentState() == RUNAWAY)
						{
							pxOwnerActor->RequestStop();
							pxOwnerActor->SetState(FLEE);
						}
						return FAILURE;
					}
				}
				else if(lineComparer.LinesIntersect(myPosition, enemyPosition, pxWallLines[i], pxWallLines[i + 1]))
				{
					if (pxOwnerActor->GetCurrentState() == CHASE)
					{
						pxOwnerActor->RequestStop();
						pxOwnerActor->SetState(SEEK);
					}
					else if (pxOwnerActor->GetCurrentState() == RUNAWAY)
					{
						pxOwnerActor->RequestStop();
						pxOwnerActor->SetState(FLEE);
					}
					return FAILURE;
				}
			}
		}
		if (pxOwnerActor->GetCurrentState() == SEEK)
		{
			pxOwnerActor->RequestStop();
			pxOwnerActor->SetState(CHASE);
		}
		else if (pxOwnerActor->GetCurrentState() == FLEE)
		{
			pxOwnerActor->RequestStop();
			pxOwnerActor->SetState(RUNAWAY);
		}
	}


	return SUCCESS;
}